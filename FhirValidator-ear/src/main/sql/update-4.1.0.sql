ALTER TABLE fhir_validator_description ADD COLUMN use_ig_fhir_server_validation boolean;

UPDATE fhir_validator_description SET use_ig_fhir_server_validation = 'false';