INSERT INTO app_configuration(id, value, variable) VALUES (nextval('app_configuration_id_seq'), '/opt/fhirvalidator/structureDefinitions', 'structure_definition_directory');

-- EncodingEnum.JSON = 0
-- EncodingEnum.XML = 1

INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
  VALUES (nextval('fhir_validator_description_id_seq'), '[ITI-83] Get Corresponding Identifiers', '1.3.6.1.4.1.12559.11.1.2.1.15.1', 'IHE',
                      'PIXm', null, false, NULL , TRUE, TRUE );
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-83] Parameters Resource XML', '1.3.6.1.4.1.12559.11.1.2.1.15.2', 'IHE',
                      'PIXm', 1, FALSE , NULL , TRUE, TRUE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-83] Parameters Resource JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.3', 'IHE',
                       'PIXm', 0, FALSE , NULL , TRUE, TRUE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-78] Query Patient Resource', '1.3.6.1.4.1.12559.11.1.2.1.15.4', 'IHE',
                      'PDQm', null, FALSE , NULL , TRUE, TRUE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-78] Query Patient Resource response XML', '1.3.6.1.4.1.12559.11.1.2.1.15.5', 'IHE',
                      'PDQm', 1, TRUE, NULL , TRUE, TRUE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'Operation Outcome XML', '1.3.6.1.4.1.12559.11.1.2.1.15.6', 'IHE', 'OperationOutcome', 1, TRUE, NULL , TRUE, TRUE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'Operation Outcome JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.7', 'IHE', 'OperationOutcome', 0, TRUE, NULL , TRUE, TRUE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-78] Query Patient Resource response JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.8', 'IHE',
                       'PDQm', 0, TRUE, NULL , TRUE, TRUE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-78] Retrieve Patient Resource', '1.3.6.1.4.1.12559.11.1.2.1.15.9', 'IHE',
                               'PDQm_READ', null, FALSE, NULL , TRUE, TRUE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-78] Retrieve Patient Resource Response XML', '1.3.6.1.4.1.12559.11.1.2.1.15.10', 'IHE',
        'PDQm_READ', 1, TRUE, NULL , TRUE, TRUE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-78] Retrieve Patient Resource Response JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.11', 'IHE',
        'PDQm_READ', 0, TRUE, NULL , TRUE, TRUE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'FHIR Resource XML (DSTU3)', '1.3.6.1.4.1.12559.11.1.2.1.15.12', 'FHIR', 'FHIR', 1, TRUE, NULL , TRUE, TRUE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'FHIR Resource JSON (DSTU3)', '1.3.6.1.4.1.12559.11.1.2.1.15.13', 'FHIR', 'FHIR', 0, TRUE, NULL , TRUE, TRUE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-65] Provide Document Bundle Request XML', '1.3.6.1.4.1.12559.11.1.2.1.15.16', 'IHE',
                  'MHD', 1, FALSE, '/opt/fhirvalidator/structureDefinitions/IHE.MHD.ProvideDocumentBundle.Comprehensive.xml' , TRUE, FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-65] Provide Document Bundle Request JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.17', 'IHE',
                  'MHD', 0, FALSE, '/opt/fhirvalidator/structureDefinitions/IHE.MHD.DocumentManifest.xml' , TRUE, FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-66] Find Document Manifests Response XML', '1.3.6.1.4.1.12559.11.1.2.1.15.18', 'IHE',
                  'MHD', 1, FALSE, '/opt/fhirvalidator/structureDefinitions/IHE.MHD.ProvideDocumentBundle.Comprehensive.xml' , TRUE, FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-66] Find Document Manifests Response JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.19', 'IHE',
        'MHD', 0, FALSE, '/opt/fhirvalidator/structureDefinitions/IHE.MHD.DocumentManifest.xml' , TRUE, FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[mCSD] Location Resource JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.20', 'IHE',
        'mCSD', 0, FALSE, '/opt/fhirvalidator/structureDefinitions/IHE.mCSD.Location.xml' , TRUE, FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[mCSD] Location Resource XML', '1.3.6.1.4.1.12559.11.1.2.1.15.21', 'IHE',
        'mCSD', 1, FALSE, '/opt/fhirvalidator/structureDefinitions/IHE.mCSD.Location.xml' , TRUE, FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[mCSD] Organization Resource JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.22', 'IHE',
        'mCSD', 0, FALSE, '/opt/fhirvalidator/structureDefinitions/IHE.mCSD.Organization.xml' , TRUE, FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[mCSD] Organization Resource XML', '1.3.6.1.4.1.12559.11.1.2.1.15.23', 'IHE',
        'mCSD', 1, FALSE, '/opt/fhirvalidator/structureDefinitions/IHE.mCSD.Organization.xml' , TRUE, FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[mCSD] Practitioner Resource JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.24', 'IHE',
        'mCSD', 0, FALSE, '/opt/fhirvalidator/structureDefinitions/IHE.mCSD.Practitioner.xml' , TRUE, FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[mCSD] Practitioner Resource XML', '1.3.6.1.4.1.12559.11.1.2.1.15.25', 'IHE',
        'mCSD', 1, FALSE, '/opt/fhirvalidator/structureDefinitions/IHE.mCSD.Practitioner.xml' , TRUE, FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[mCSD] Practitioner Role Resource JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.14', 'IHE',
        'mCSD', 0, FALSE, '/opt/fhirvalidator/structureDefinitions/IHE.mCSD.PractitionerRole.xml' , TRUE, FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, format, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[mCSD] Practitioner Role Resource XML', '1.3.6.1.4.1.12559.11.1.2.1.15.15', 'IHE',
        'mCSD', 1, FALSE, '/opt/fhirvalidator/structureDefinitions/IHE.mCSD.PractitionerRole.xml' , TRUE, FALSE);
