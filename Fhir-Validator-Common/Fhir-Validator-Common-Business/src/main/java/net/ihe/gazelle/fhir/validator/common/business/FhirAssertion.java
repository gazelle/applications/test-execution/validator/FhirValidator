package net.ihe.gazelle.fhir.validator.common.business;

import net.ihe.gazelle.validation.Assertion;

import java.util.List;

/**
 * <p>FhirAssertion interface.</p>
 *
 * @author aberge
 * @version 1.0: 21/11/17
 */
public interface FhirAssertion {

    String getDescription();

    String getIdentifier();

    String getLocation();

    List<Assertion> getTestedAssertion();
}
