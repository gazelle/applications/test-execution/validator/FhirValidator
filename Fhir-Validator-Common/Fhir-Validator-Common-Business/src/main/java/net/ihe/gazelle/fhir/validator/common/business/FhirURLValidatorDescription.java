package net.ihe.gazelle.fhir.validator.common.business;

import ca.uhn.fhir.rest.api.EncodingEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>FhirRequestValidatorDescription class.</p>
 *
 * @author abe
 * @version 1.0: 27/08/18
 */

public class FhirURLValidatorDescription extends FhirValidatorDescription {

    private String resourceName;

    private String operation;

    private List<FhirRequestParameter> requestParameters;


    public FhirURLValidatorDescription(){
        super();
        this.requestParameters = new ArrayList<FhirRequestParameter>();
    }

    public FhirURLValidatorDescription(FhirURLValidatorDescription original){
        super(original);
        this.requestParameters = new ArrayList<FhirRequestParameter>(original.getRequestParameters());
        this.resourceName = original.getResourceName();
        this.operation = original.getOperation();
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public List<FhirRequestParameter> getRequestParameters() {
        return requestParameters;
    }

    public void setRequestParameters(List<FhirRequestParameter> requestParameters) {
        this.requestParameters = requestParameters;
    }

    @Override
    public EncodingEnum getFormat(){
        return null; // always URLs
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FhirURLValidatorDescription)) {
            return false;
        }

        FhirURLValidatorDescription that = (FhirURLValidatorDescription) o;

        if (resourceName != null ? !resourceName.equals(that.resourceName) : that.resourceName != null) {
            return false;
        }
        if (operation != null ? !operation.equals(that.operation) : that.operation != null) {
            return false;
        }
        return requestParameters != null ? requestParameters.equals(that.requestParameters) : that.requestParameters == null;
    }

    @Override
    public int hashCode() {
        int result = resourceName != null ? resourceName.hashCode() : 0;
        result = 31 * result + (operation != null ? operation.hashCode() : 0);
        result = 31 * result + (requestParameters != null ? requestParameters.hashCode() : 0);
        return result;
    }

    public boolean isOperationDefined() {
        return this.operation != null && !this.operation.isEmpty();
    }
}

