package net.ihe.gazelle.fhir.validator.common.business;

/**
 * <p>CommonFhirRequestParameter enum.</p>
 *
 * @author abe
 * @version 1.0: 28/08/18
 */
public enum CommonFhirRequestParameter implements IFhirRequestParameter {

    ID ("_id", ParameterType.TOKEN, null),
    LAST_UPDATED("_lastUpdated", ParameterType.INSTANT, null),
    TAG("_tag", ParameterType.TOKEN, null),
    PROFILE("_profile", ParameterType.URI, null),
    SECURITY("_security", ParameterType.TOKEN, null),
    TEXT("_text", ParameterType.STRING, null),
    CONTENT("_content", ParameterType.STRING, null),
    LIST("_list", ParameterType.STRING, null),
    QUERY("_query", ParameterType.STRING, null),
    SORT("_sort", ParameterType.STRING, null),
    COUNT("_count", ParameterType.NUMBER, ValidatorConstants.POSITIVE_INT_REGEX),
    INCLUDE("_include", ParameterType.STRING, null),
    REVINCLUDE("_revinclude", ParameterType.STRING, null),
    SUMMARY("_summary", ParameterType.STRING, "true|false"),
    CONTAINED("_contained", ParameterType.STRING, "true|false|both"),
    CONTAINEDTYPE("_containedType", ParameterType.STRING, "container|contained"),
    FORMAT("_format", ParameterType.STRING, "xml|text/xml|application/xml|application/fhir\\+xml|json|application/json|application/fhir\\+json");

    CommonFhirRequestParameter(String inName, ParameterType inType, String inRegex){
        this.name = inName;
        this.type = inType;
        this.regex = inRegex;
    }

    String name;
    String regex;
    ParameterType type;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public ParameterType getType() {
        return this.type;
    }

    @Override
    public String getRegex() {
        return this.regex;
    }

    @Override
    public String getNameWithPrefix() {
        return this.name;
    }

    public static CommonFhirRequestParameter getParameterByName(String inName){
        for (CommonFhirRequestParameter parameter: values()){
            if (parameter.getName().equals(inName)){
                return parameter;
            }
        }
        return null;
    }
}
