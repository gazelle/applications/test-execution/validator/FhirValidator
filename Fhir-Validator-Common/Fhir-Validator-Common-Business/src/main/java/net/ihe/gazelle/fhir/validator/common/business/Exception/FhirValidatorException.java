package net.ihe.gazelle.fhir.validator.common.business.Exception;

/**
 * <p>FhirValidatorException class.</p>
 *
 * @author abe
 * @version 1.0: 04/01/18
 */

public class FhirValidatorException extends Exception {

    public FhirValidatorException(Exception cause){
        super(cause);
    }

    public FhirValidatorException(String message){
        super(message);
    }

    public FhirValidatorException(String message, Exception cause){
        super(message, cause);
    }

    public FhirValidatorException() {

    }
}
