package net.ihe.gazelle.fhir.validator.common.adapter;

import net.ihe.gazelle.fhir.validator.common.business.FhirParameterPrefix;
import net.ihe.gazelle.fhir.validator.common.business.IFhirRequestParameter;
import net.ihe.gazelle.fhir.validator.common.business.ParameterType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * <p>FhirRequestParameter class.</p>
 *
 * @author abe
 * @version 1.0: 27/08/18
 */

@Entity
@Table(name = "fhir_request_parameter", schema = "public")
@SequenceGenerator(name = "fhir_request_parameter_sequence", sequenceName = "fhir_request_parameter_id_seq", allocationSize = 1)
public class FhirRequestParameter implements IFhirRequestParameter, Serializable {

    @Id
    @GeneratedValue(generator = "fhir_request_parameter_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "is_required")
    private boolean required;

    @Column(name = "regex")
    private String regex;

    @Enumerated(EnumType.STRING)
    @Column(name = "prefix")
    private FhirParameterPrefix prefix;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private ParameterType type;

    @ManyToOne(targetEntity = FhirURLValidatorDescription.class, cascade = CascadeType.MERGE)
    @JoinColumn(name = "validator_description_id")
    private FhirURLValidatorDescription validatorDescription;

    @OneToMany(targetEntity = ValueSetForParameter.class, mappedBy = "requestParameter", cascade = CascadeType.MERGE)
    private List<ValueSetForParameter> allowedListsOfCodes;

    public FhirRequestParameter(){
        this.required = false;
        this.type = ParameterType.STRING;
    }

    public FhirRequestParameter(FhirURLValidatorDescription inValidatorDescription){
        this.validatorDescription = inValidatorDescription;
        this.required = false;
        this.type = ParameterType.STRING;
    }

    public FhirRequestParameter(String inName, ParameterType inType, boolean isRequired, String inRegex) {
        this.name = inName;
        this.type = inType;
        this.required = isRequired;
        this.regex = inRegex;
    }


    public FhirParameterPrefix getPrefix() {
        return prefix;
    }

    public void setPrefix(FhirParameterPrefix prefix) {
        this.prefix = prefix;
    }

    @Override
    public ParameterType getType() {
        return type;
    }

    public void setType(ParameterType type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public FhirURLValidatorDescription getValidatorDescription() {
        return validatorDescription;
    }

    public void setValidatorDescription(FhirURLValidatorDescription validatorDescription) {
        this.validatorDescription = validatorDescription;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }


    @Override
    public String getRegex() {
        return regex;
    }

    @Override
    public String getNameWithPrefix() {
        String prefixedName = this.name;
        if (prefix != null){
            prefixedName = prefixedName.concat(":").concat(prefix.getPrefixValue());
        }
        return prefixedName;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FhirRequestParameter)) {
            return false;
        }

        FhirRequestParameter that = (FhirRequestParameter) o;

        if (required != that.required) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return regex != null ? regex.equals(that.regex) : that.regex == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (required ? 1 : 0);
        result = 31 * result + (regex != null ? regex.hashCode() : 0);
        return result;
    }

    public List<ValueSetForParameter> getAllowedListsOfCodes() {
        return allowedListsOfCodes;
    }

    public void setAllowedListsOfCodes(List<ValueSetForParameter> allowedListsOfCodes) {
        this.allowedListsOfCodes = allowedListsOfCodes;
    }
}
