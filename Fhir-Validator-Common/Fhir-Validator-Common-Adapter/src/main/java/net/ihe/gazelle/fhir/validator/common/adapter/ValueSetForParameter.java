package net.ihe.gazelle.fhir.validator.common.adapter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>ValueSet class.</p>
 *
 * @author abe
 * @version 1.0: 05/09/18
 */

@Entity
@Table(name = "fhir_value_set_for_parameter", schema = "public")
@SequenceGenerator(name = "fhir_value_set_for_parameter_sequence", sequenceName = "fhir_value_set_for_parameter_id_seq", allocationSize = 1)
public class ValueSetForParameter implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(generator = "fhir_value_set_for_parameter_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "namespace_uri")
    @NotNull
    private String namespaceURI;

    @Column(name = "value_set_oid")
    private String valueSetOid;

    @Column(name = "is_extensible")
    private boolean extensible;

    @ManyToOne(cascade = CascadeType.MERGE, targetEntity = FhirRequestParameter.class)
    @JoinColumn(name = "request_parameter")
    private FhirRequestParameter requestParameter;

    public ValueSetForParameter() {
        //Empty Constructor
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNamespaceURI() {
        return namespaceURI;
    }

    public void setNamespaceURI(String namespaceURI) {
        this.namespaceURI = namespaceURI;
    }

    public String getValueSetOid() {
        return valueSetOid;
    }

    public void setValueSetOid(String valueSetOid) {
        this.valueSetOid = valueSetOid;
    }

    public boolean isExtensible() {
        return extensible;
    }

    public void setExtensible(boolean extensible) {
        this.extensible = extensible;
    }

    public FhirRequestParameter getRequestParameter() {
        return requestParameter;
    }

    public void setRequestParameter(FhirRequestParameter requestParameter) {
        this.requestParameter = requestParameter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ValueSetForParameter)) {
            return false;
        }

        ValueSetForParameter that = (ValueSetForParameter) o;

        if (extensible != that.extensible) {
            return false;
        }
        if (namespaceURI != null ? !namespaceURI.equals(that.namespaceURI) : that.namespaceURI != null) {
            return false;
        }
        return valueSetOid != null ? valueSetOid.equals(that.valueSetOid) : that.valueSetOid == null;
    }

    @Override
    public int hashCode() {
        int result = namespaceURI != null ? namespaceURI.hashCode() : 0;
        result = 31 * result + (valueSetOid != null ? valueSetOid.hashCode() : 0);
        result = 31 * result + (extensible ? 1 : 0);
        return result;
    }
}
