package net.ihe.gazelle.fhir.validator.common.adapter;

import net.ihe.gazelle.fhir.validator.common.business.Exception.FhirValidatorException;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * <p>FhirResouceValidatorDescription class.</p>
 *
 * @author abe
 * @version 1.0: 27/08/18
 */

@Entity
@DiscriminatorValue("resource_val")
public class FhirResourceValidatorDescription extends FhirValidatorDescription{

    private static final String EMPTY_STRING = "";

    @Column(name = "execute_schematron_validation")
    private boolean executeSchematronValidation;

    @Column(name = "custom_structure_definition")
    private String customStructureDefinition;

    @Column(name = "execute_schema_validation")
    private boolean executeSchemaValidation;

    @Column(name = "use_ig_fhir_server_validation")
    private boolean useIGFhirService;

    @Column(name = "structure_definition_id")
    private String structureDefinitionUrl;

    @Column(name = "structure_context_path")
    private String contextPath;

    @Column(name = "base_definition")
    private String baseDefinition;

    /**
     * This attribute is used to determine the order in which the custom definition shall be load in the ValidationSupport class
     * because they are dependencies between them, we need to be sure the structueDefinition used as base are already
     */
    @Column(name = "weight")
    private Integer weight;

    public FhirResourceValidatorDescription(){
        super();
        executeSchematronValidation = false;
        executeSchemaValidation = true;
    }

    public FhirResourceValidatorDescription(FhirResourceValidatorDescription original){
        super(original);
        this.executeSchematronValidation = original.isExecuteSchematronValidation();
        this.executeSchemaValidation = original.isExecuteSchemaValidation();
        this.structureDefinitionUrl = original.getStructureDefinitionUrl();
        this.contextPath = original.getContextPath();
        this.baseDefinition = original.getBaseDefinition();
        this.customStructureDefinition = original.getCustomStructureDefinition();
    }

    public FhirResourceValidatorDescription(String name, String oid, String descriminator, String profile, String structDefFilePath) throws FhirValidatorException {
        super(name, oid, descriminator, profile);
        this.customStructureDefinition = structDefFilePath;
    }

    public FhirResourceValidatorDescription(String name, String oid, String descriminator, String profile) throws FhirValidatorException {
        super(name, oid, descriminator, profile);
    }

    public String getSnapshotFilePath(){
        if (this.customStructureDefinition != null && !this.customStructureDefinition.isEmpty()) {
            return customStructureDefinition.replace(".xml", ".snapshot.xml");
        } else {
            return null;
        }
    }

    public void setExecuteSchemaValidation(boolean executeSchemaValidation) {
        this.executeSchemaValidation = executeSchemaValidation;
    }

    public String getStructureDefinitionUrl() {
        return structureDefinitionUrl;
    }

    public void setStructureDefinitionUrl(String structureDefinitionUrl) {
        this.structureDefinitionUrl = structureDefinitionUrl;
    }

    public String getBaseDefinition() {
        return baseDefinition;
    }

    public void setBaseDefinition(String baseDefinition) {
        this.baseDefinition = baseDefinition;
    }

    public boolean isExecuteSchematronValidation() {
        return executeSchematronValidation;
    }

    public void setExecuteSchematronValidation(boolean executeSchematronValidation) {
        this.executeSchematronValidation = executeSchematronValidation;
    }

    /**
     * Getter for the useIGFhirService property.
     *
     * @return the value of the property.
     */
    public boolean isUseIGFhirService() {
        return useIGFhirService;
    }

    /**
     * Setter for the useIGFhirService property.
     *
     * @param useIGFhirService value to set to the property.
     */
    public void setUseIGFhirService(boolean useIGFhirService) {
        this.useIGFhirService = useIGFhirService;
    }

    public String getCustomStructureDefinition() {
        return customStructureDefinition;
    }

    public void setCustomStructureDefinition(String customStructureDefinition) {
        if (EMPTY_STRING.equals(customStructureDefinition)){
            this.customStructureDefinition = null;
        } else {
            this.customStructureDefinition = customStructureDefinition;
        }
    }

    public boolean isExecuteSchemaValidation() {
        return this.executeSchemaValidation;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public boolean hasCustomStructureDefinition() {
        return (this.customStructureDefinition != null && !this.customStructureDefinition.isEmpty());
    }


    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }


}