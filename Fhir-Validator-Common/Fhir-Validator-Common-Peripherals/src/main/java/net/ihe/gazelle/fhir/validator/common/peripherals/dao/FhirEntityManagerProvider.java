package net.ihe.gazelle.fhir.validator.common.peripherals.dao;

import net.ihe.gazelle.hql.providers.detached.AbstractEntityManagerProvider;
import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.ContextNotActiveException;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

/**
 * <p>SimulatorEntityManagerProvider class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Named
public class FhirEntityManagerProvider extends AbstractEntityManagerProvider {

	private static final Logger log = LoggerFactory.getLogger(FhirEntityManagerProvider.class);
	private static final int LIGHT_WEIGHT = -100;
	private static final int HEAVY_WEIGHT = 100;

	@Inject
	BeanManager beanManager;

	/** {@inheritDoc} */
	@Override
	public Integer getWeight() {
		log.debug("BeanManager is null : {}", beanManager != null);
		try {
			if (beanManager != null && beanManager.getContext(ApplicationScoped.class).isActive()) {
				return LIGHT_WEIGHT;
			} else {
				return HEAVY_WEIGHT;
			}
		} catch (final ContextNotActiveException | NullPointerException e) {
			log.error("Error checking context :", e);
			return HEAVY_WEIGHT;
		}
	}

	/** {@inheritDoc} */
	@Override
	public String getHibernateConfigPath() {
		return "META-INF/hibernate.cfg.xml";
	}

	@Override
	public EntityManager provideEntityManager() {
        HibernateActionPerformer.ENTITYMANGER_THREADLOCAL.remove();
		return super.provideEntityManager();
	}
}
