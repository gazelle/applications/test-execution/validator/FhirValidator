package net.ihe.gazelle.fhir.validator.common.peripherals.hapi;

import ca.uhn.fhir.parser.DataFormatException;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.EncodingEnum;
import net.ihe.gazelle.fhir.constants.FhirParserProvider;
import net.ihe.gazelle.fhir.validator.common.business.Exception.FhirValidatorException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.OperationOutcome;

import java.io.InputStreamReader;

/**
 * Adapter for Hapi library :
 * Parsing of resources
 */
public class HapiParserAdapter {

    /**
     * Default private constructor for the class
     */
    private HapiParserAdapter() {
        //Empty
    }

    /**
     * Parse an OperationOutcome from a literal operation outcome
     *
     * @param literalOperationOutcome literal representation of the operation outcome
     * @param format of the literal operation outcome
     * @return OperationOutcome extracted from the literal operation outcome
     * @throws FhirValidatorException in case the parser doesn't work
     */
    public static OperationOutcome parseOperationOutcome(String literalOperationOutcome, String format) throws FhirValidatorException {
        OperationOutcome libraryOperationOutcome;
        try {
            IParser parser = FhirParserProvider.getParserForEncoding(EncodingEnum.forContentType(format));
            libraryOperationOutcome = parser.parseResource(OperationOutcome.class, literalOperationOutcome);
        } catch (DataFormatException e) {
            throw new FhirValidatorException(e);
        }
        return libraryOperationOutcome;
    }

    /**
     * Parse XML Base Resource from a reader
     *
     * @param reader reader to parse
     * @return the parsed resource
     */
    public static IBaseResource parseXMLBaseResource(InputStreamReader reader) {
        IParser parser = FhirParserProvider.getXmlParser();
        return parser.parseResource(reader);
    }

}
