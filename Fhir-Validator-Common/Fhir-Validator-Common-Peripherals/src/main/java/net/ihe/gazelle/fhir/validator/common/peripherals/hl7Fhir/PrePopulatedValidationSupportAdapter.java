package net.ihe.gazelle.fhir.validator.common.peripherals.hl7Fhir;

import ca.uhn.fhir.context.FhirContext;
import org.hl7.fhir.instance.model.api.IBaseResource;

import java.util.Map;

public class PrePopulatedValidationSupportAdapter extends org.hl7.fhir.common.hapi.validation.support.PrePopulatedValidationSupport {

    public PrePopulatedValidationSupportAdapter(FhirContext theContext) {
        super(theContext);
    }

    public PrePopulatedValidationSupportAdapter(FhirContext theFhirContext, Map<String, IBaseResource> theStructureDefinitions, Map<String,
            IBaseResource> theValueSets, Map<String, IBaseResource> theCodeSystems) {
        super(theFhirContext, theStructureDefinitions, theValueSets, theCodeSystems);
    }
}
