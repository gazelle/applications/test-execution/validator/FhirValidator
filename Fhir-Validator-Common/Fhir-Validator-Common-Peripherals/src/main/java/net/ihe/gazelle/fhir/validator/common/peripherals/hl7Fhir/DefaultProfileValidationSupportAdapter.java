package net.ihe.gazelle.fhir.validator.common.peripherals.hl7Fhir;

import ca.uhn.fhir.context.FhirContext;

public class DefaultProfileValidationSupportAdapter extends ca.uhn.fhir.context.support.DefaultProfileValidationSupport {

    /**
     * Constructor
     *
     * @param theFhirContext The context to use
     */
    public DefaultProfileValidationSupportAdapter(FhirContext theFhirContext) {
        super(theFhirContext);
    }
}
