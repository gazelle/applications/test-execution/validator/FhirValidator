package net.ihe.gazelle.fhir.validator.common.peripherals.hl7Fhir;

import net.ihe.gazelle.fhir.validator.common.business.Exception.FhirValidatorException;
import net.ihe.gazelle.fhir.validator.common.peripherals.hapi.HapiParserAdapter;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.hl7.fhir.r4.model.StringType;

/**
 * Adapter to do operations regarding OperationOutcomes
 */
public class OperationOutcomeAdapter {
    private OperationOutcome libraryOperationOutcome;

    /**
     * Operation outcome constructor to parse an OperationOutcome from a message
     * @param message the message to build from
     * @param format of the message
     * @throws FhirValidatorException an OperationOutcomeAdapter to do operations regarding OperationOutcomes
     */
    public OperationOutcomeAdapter(String message, String format) throws FhirValidatorException {
        this.libraryOperationOutcome = HapiParserAdapter.parseOperationOutcome(message, format);
    }

    /**
     * check if the severity is at the Error level
     * @return true if the severity is at the Error level
     */
    public boolean checkSeverityIsError() {
        boolean severityIsError = false;
        if (this.libraryOperationOutcome != null) {
            severityIsError = OperationOutcome.IssueSeverity.ERROR.equals(libraryOperationOutcome.getIssueFirstRep().getSeverity());
        }
        return severityIsError;
    }

    /**
     * check that a diagnostic element is present and not empty
     * @return true if the diagnostic element is valid
     */
    public boolean isDiagnosticElementValid() {
        StringType diagnosticsElement = libraryOperationOutcome.getIssueFirstRep().getDiagnosticsElement();
        return diagnosticsElement != null && !diagnosticsElement.isEmpty();
    }
}
