package net.ihe.gazelle.fhir.validator.common.peripherals.hapi;

import ca.uhn.fhir.rest.api.EncodingEnum;
import net.ihe.gazelle.fhir.validator.common.business.Exception.FhirValidatorException;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertNotNull;

public class HapiParserAdapterTest {

    /**
     * check parseOutcome returns an OperationOutcome
     * @throws IOException
     * @throws FhirValidatorException
     */
    @Test
    public void testParseOutcome() throws IOException, FhirValidatorException {
        String format = EncodingEnum.XML_PLAIN_STRING;
        String message = this.readFile("src/test/resources/net.ihe.gazelle.fhir.operationoutcome/operationoutcome-ok.xml");
        OperationOutcome outcome = HapiParserAdapter.parseOperationOutcome(message,format);
        assertNotNull(outcome);
    }

    public static String readFile(String path) throws IOException {
        FileInputStream fis = new FileInputStream(path);
        byte[] encoded = new byte[fis.available()];
        fis.read(encoded);
        return new String(encoded, StandardCharsets.UTF_8);
    }
}
