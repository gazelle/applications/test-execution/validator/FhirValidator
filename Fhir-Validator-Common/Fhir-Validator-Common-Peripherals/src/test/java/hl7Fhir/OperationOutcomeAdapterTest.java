package hl7Fhir;

import ca.uhn.fhir.rest.api.EncodingEnum;

import net.ihe.gazelle.fhir.validator.common.peripherals.hl7Fhir.OperationOutcomeAdapter;
import net.ihe.gazelle.fhir.validator.common.business.Exception.FhirValidatorException;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

/**
 * test library hl7Fhir OperationOutcome adapter
 */
public class OperationOutcomeAdapterTest {

    /**
     * default format
     */
    String format = EncodingEnum.XML_PLAIN_STRING;

    /**
     * default message
     */
    String message = this.readFile("src/test/resources/net.ihe.gazelle.fhir.operationoutcome/operationoutcome-ok.xml");

    /**
     * adapter declaration
     */
    OperationOutcomeAdapter operationOutcomeAdapter;

    public OperationOutcomeAdapterTest() throws IOException, FhirValidatorException {

    }

    /**
     * Create a new Adapter from message loaded
     * @param filePath to load message from
     */
    private void prepare(String filePath) {
        try {
            operationOutcomeAdapter = new OperationOutcomeAdapter(this.readFile(filePath), format);
        } catch (Exception e) {
            // should never happen
        }
    }

    /**
     * test adapter creation is ok with valid message
     * @throws IOException for readFile
     */
    @Test
    public void getOperationFromMessageValid() throws IOException, FhirValidatorException {
        String message = this.readFile("src/test/resources/net.ihe.gazelle.fhir.operationoutcome/operationoutcome-ok.xml");
        operationOutcomeAdapter = new OperationOutcomeAdapter(message, format);
        assertNotNull(operationOutcomeAdapter);
    }

    /**
     * test adapter creation throws DataFormatException with invalid message
     * @throws IOException for readFile
     */
    @Test(expected= FhirValidatorException.class)
    public void getOperationFromMessageInValidMessage() throws FhirValidatorException {
        String message = "invalid message";
        operationOutcomeAdapter = new OperationOutcomeAdapter(message, format);
        assertNull(operationOutcomeAdapter);
    }

    /**
     * test checkSeverityIsError returns true when severity is ERROR
     * @throws IOException for readFile
     */
    @Test
    public void checkSeverityIsErrorTrue() {
        this.prepare("src/test/resources/net.ihe.gazelle.fhir.operationoutcome/operationoutcome-ok.xml");
        assertTrue(operationOutcomeAdapter.checkSeverityIsError());
    }

    /**
     * test checkSeverityIsError returns false when severity is not ERROR
     * @throws IOException for readFile
     */
    @Test
    public void checkSeverityIsErrorFalse() {
        this.prepare("src/test/resources/net.ihe.gazelle.fhir.operationoutcome/operationoutcome-003-ko.xml");
        assertFalse(operationOutcomeAdapter.checkSeverityIsError());
    }

    /**
     * test isDiagnosticElementValid returns true when element is valid
     * @throws IOException for readFile
     */
    @Test
    public void diagnosticElementValid() {
        this.prepare("src/test/resources/net.ihe.gazelle.fhir.operationoutcome/operationoutcome-ok.xml");
        assertTrue(operationOutcomeAdapter.isDiagnosticElementValid());
    }

    /**
     * test isDiagnosticElementValid returns false when element is invalid
     * @throws IOException for readFile
     */
    @Test
    public void diagnosticElementInvalid() {
        this.prepare("src/test/resources/net.ihe.gazelle.fhir.operationoutcome/operationoutcome-001-ko.xml");
        assertFalse(operationOutcomeAdapter.isDiagnosticElementValid());
    }

    public static String readFile(String path) throws IOException {
        FileInputStream fis = new FileInputStream(path);
        byte[] encoded = new byte[fis.available()];
        fis.read(encoded);
        return new String(encoded, StandardCharsets.UTF_8);
    }


}
