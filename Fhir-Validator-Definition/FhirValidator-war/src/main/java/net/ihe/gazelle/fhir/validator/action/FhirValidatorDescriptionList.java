package net.ihe.gazelle.fhir.validator.action;

import net.ihe.gazelle.fhir.validator.common.adapter.FhirResourceValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.adapter.FhirURLValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.adapter.FhirValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.adapter.FhirValidatorDescriptionQuery;
import net.ihe.gazelle.fhir.validator.common.peripherals.ValidationSupportProvider;
import net.ihe.gazelle.filter.Filter;
import net.ihe.gazelle.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.preferences.GenericConfigurationManager;
import net.ihe.gazelle.validator.definition.adapter.auth.Pages;
import net.ihe.gazelle.validator.definition.adapter.webservice.GazelleFhirValidationDescriptionWS;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 * <p>FhirValidatorDescriptionManager class.</p>
 *
 * @author abe
 * @version 1.0: 04/01/18
 */
@ManagedBean(name = "fhirValidatorDescriptionList")
@ViewScoped
public class FhirValidatorDescriptionList extends FhirvalidatorDescriptionManager {

    private Filter<FhirValidatorDescription> filter;

    private FilterDataModel<FhirValidatorDescription> allFhirValidatorDescriptions;

    @Inject
    public void setFhirValidatorDescriptionTransactionManager(GazelleFhirValidationDescriptionWS fhirValidatorDescriptionWebService){
        this.fhirValidatorDescriptionWebService = fhirValidatorDescriptionWebService;
    }

    @Inject
    public void setApplicationConfigurationManager(GenericConfigurationManager genericConfigurationManager){
        this.genericConfigurationManager = genericConfigurationManager;
    }

    public Filter<FhirValidatorDescription> getFilter() {
        if (filter == null){
            filter = new Filter<FhirValidatorDescription>(getCriterionForFilter());
        }
        return filter;
    }

    private HQLCriterionsForFilter<FhirValidatorDescription> getCriterionForFilter() {
        FhirValidatorDescriptionQuery query = new FhirValidatorDescriptionQuery();
        HQLCriterionsForFilter<FhirValidatorDescription> criterionsForFilter = query.getHQLCriterionsForFilter();
        return criterionsForFilter;
    }

    public FilterDataModel<FhirValidatorDescription> getAllFhirValidatorDescriptions(){
        if (allFhirValidatorDescriptions == null){
            allFhirValidatorDescriptions = new FilterDataModel<FhirValidatorDescription>(getFilter()) {
                @Override
                protected Object getId(FhirValidatorDescription validatorDescription) {
                    return validatorDescription.getId();
                }
            };
        }
        return allFhirValidatorDescriptions;
    }

    public String newResourceValidatorDescription(){
        return genericConfigurationManager.getApplicationUrl() + "/validators/editResourceValidator.seam?id=new";
    }

    public String newURLValidatorDescription(){
        return genericConfigurationManager.getApplicationUrl() + "/validators/editURLValidator.seam?id=new";
    }

    public String copyValidatorDescription(FhirValidatorDescription entry){
        if (entry != null){
            String appUrl = genericConfigurationManager.getApplicationUrl();
            if (entry instanceof FhirResourceValidatorDescription) {
                return appUrl + "/validators/editResourceValidator.seam?copy=true&id=" + entry.getOid();
            } else {
                return appUrl + "/validators/editURLValidator.seam?copy=true&id=" + entry.getOid();
            }
        } else {
            return null;
        }
    }

    public void reloadStructureDefinitionsAndValueSets(){
        int numberOfRaisedExceptions = ValidationSupportProvider.loadStructureDefinitionsAndTerminologies();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        FacesMessage facesMessage;
        if (numberOfRaisedExceptions > 0) {
            facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, String.format("%s exception(s) have been raised during the reloading of resources. Please see application logs.", numberOfRaisedExceptions),null);
        } else {
            facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Reload structure definitions and value sets",
                    "Structure definitions and value sets have been reloaded.");
        }
        facesContext.addMessage(null, facesMessage);
    }

    public void setUnavailable(FhirValidatorDescription entry){
        if (entry != null){
            entry.setAvailable(false);
            saveEntry(entry);
        }
    }

    private void saveEntry(FhirValidatorDescription entry) {
        fhirValidatorDescriptionWebService.saveFhirValidatorDescription(entry);
    }

    public void setAvailable(FhirValidatorDescription entry){
        if (entry != null){
            entry.setAvailable(true);
            saveEntry(entry);
        }
    }

    /**
     * return the type of the validator
     * @param entry the validator
     * @return the type of the validator
     */
    public String getValidatorType(Object entry) {
        if(entry instanceof FhirResourceValidatorDescription) {
            return "RES";
        }
        if(entry instanceof FhirURLValidatorDescription) {
            return "URL";
        }
        return null;
    }

    @Override
    public String getDisplayPage() {
        return Pages.VALIDATOR_LIST.getLink();
    }

    @Override
    protected Class getEntityClass() {
        return null;
    }
}
