package net.ihe.gazelle.fhir.validator.action;

import net.ihe.gazelle.fhir.validator.common.adapter.FhirResourceValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.adapter.FhirValidatorDescription;
import net.ihe.gazelle.preferences.GenericConfigurationManager;
import net.ihe.gazelle.validator.definition.adapter.auth.Pages;
import net.ihe.gazelle.validator.definition.adapter.webservice.GazelleFhirValidationDescriptionWS;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;

/**
 * <p>FhirValidatorDescriptionDisplay class.</p>
 *
 * @author abe
 * @version 1.0: 09/04/18
 */
@ManagedBean(name = "fhirResourceValidatorDescriptionEdition")
@ViewScoped
public class FhirResourceValidatorDescriptionEdition extends FhirvalidatorDescriptionManager<FhirResourceValidatorDescription> {

    @Inject
    public void setFhirValidatorDescriptionTransactionManager(GazelleFhirValidationDescriptionWS fhirValidatorDescriptionWebService){
        this.fhirValidatorDescriptionWebService = fhirValidatorDescriptionWebService;
    }

    @Inject
    public void setApplicationConfigurationManager(GenericConfigurationManager genericConfigurationManager){
        this.genericConfigurationManager = genericConfigurationManager;
    }

    @PostConstruct
    public void initialize(){
        String id = getIdParameterFromUrl();
        if ("new".equals(id)){
            setValidatorDescription(new FhirResourceValidatorDescription());
            paramId = "new";
        } else {
            String copy = getParameterFromUrl("copy");
            if ("true".equals(copy)){
                paramId = "copy";
                FhirResourceValidatorDescription validatorDescription = getValidatorForIdParameter(id);
                if (validatorDescription != null) {
                    setValidatorDescription(new FhirResourceValidatorDescription(validatorDescription));
                } else {
                    FacesContext facesContext = FacesContext.getCurrentInstance();
                    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error matching id",
                            "No entry matches id " + id);
                    facesContext.addMessage(null, facesMessage);
                }
            } else {
                paramId = id;
                setValidatorDescription(getValidatorForIdParameter(id));
            }
        }
    }


    @Override
    protected Class<FhirResourceValidatorDescription> getEntityClass() {
        return FhirResourceValidatorDescription.class;
    }

    @Override
    public String getDisplayPage() {
        return Pages.DISPLAY_RESOURCE_VALIDATOR.getLink();
    }

}
