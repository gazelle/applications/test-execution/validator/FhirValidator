package net.ihe.gazelle.validator.definition.adapter.dao;


import net.ihe.gazelle.fhir.validator.common.adapter.FhirRequestParameter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.validator.definition.application.FhirRequestParameterDAO;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Named
public class FhirRequestParameterDAOImpl implements FhirRequestParameterDAO {

    @PersistenceContext(unitName = "FhirValidator-Common-PersistenceUnit")
    private static EntityManager entityManager;

    private static EntityManager getEntityManager() {
        if (entityManager == null) {
            return EntityManagerService.provideEntityManager();
        } else {
            return entityManager;
        }
    }

    @Override
    @Transactional
    public void deleteFhirRequestParameter(String ParameterId){
        EntityManager entityManager = getEntityManager();
        FhirRequestParameter toDeleteFromBase = entityManager.find(FhirRequestParameter.class, Integer.valueOf(ParameterId));
        entityManager.remove(toDeleteFromBase);
    }
}
