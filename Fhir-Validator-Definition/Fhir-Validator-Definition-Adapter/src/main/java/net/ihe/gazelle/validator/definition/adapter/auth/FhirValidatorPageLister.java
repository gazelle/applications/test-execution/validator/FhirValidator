package net.ihe.gazelle.validator.definition.adapter.auth;

import net.ihe.gazelle.pages.Page;
import net.ihe.gazelle.pages.PageLister;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

@Named("pageLister")
public class FhirValidatorPageLister implements PageLister {

    @Override
    public Collection<Page> getPages() {
        Collection<Page> pages = new ArrayList<>();
        pages.addAll(Arrays.asList(Pages.values()));
        return pages;
    }
}
