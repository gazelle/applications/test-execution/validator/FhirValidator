package net.ihe.gazelle.validator.definition.adapter.webservice;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * our Application
 */
@ApplicationPath("/")
public class WSApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(GazelleFhirValidationDescriptionWS.class);
        return s;

    }

    @Override
    public Set<Object> getSingletons() {
        Set<Object> s = new HashSet<>();
        return s;
    }
}
