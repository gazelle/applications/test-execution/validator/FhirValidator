package net.ihe.gazelle.fhir;

import net.ihe.gazelle.fhir.validator.common.business.FhirRequestParameter;
import net.ihe.gazelle.fhir.validator.common.business.FhirURLValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.business.ParameterModifier;
import net.ihe.gazelle.fhir.validator.common.business.ParameterType;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>FhirURLValidatorTestData class.</p>
 *
 * @author abe
 * @version 1.0: 29/08/18
 */

public class FhirURLValidatorTestData {


    private static final String PIXM_OPERATION = "ihe-pix";

    private FhirURLValidatorTestData() {

    }

    static final String URL1_FULL = "http://baseurl.com/Patient?given=abc";
    static final String URL1_BASE = "http://baseurl.com/Patient";
    static final String URL1_PARAMETERS = "given=abc";
    static final String URL2_FULL = "http://baseurl.com/Patient?identifier=urn:oid:1.2.3.4|145&identifier=urn:oid:2.16.840.1.113883.4.1|123456789";
    static final String URL3_FULL = "http://example.com/base/Slot?_include=Slot:schedule&_include:iterate=Schedule:actor&start=ge 2019-01-02&start=le20190106&schedule.actor:PractitionerRole.specialty=urn:oid:1.2.250.1.213.2.28|SM54&schedule.actor:PractitionerRole.address=Paris&status=free";
    static final String URL2_BASE = "http://baseurl.com/Patient";
    static final String URL2_PARAMETERS = "identifier=urn:oid:1.2.3.4|145&identifier=urn:oid:2.16.840.1.113883.4.1|123456789";
    static final String URL2_FIRST_PARAMETER = "identifier=urn:oid:1.2.3.4|145";
    static final String URL2_SECOND_PARAMETER = "identifier=urn:oid:2.16.840.1.113883.4.1|123456789";
    static final String URL2_SECOND_PARAMETER_VALUE = "urn:oid:2.16.840.1.113883.4.1|123456789";
    static final String URL2_SECOND_PARAMETER_KEY = "identifier";
    static final String KEY_WITH_CONTAINS_MODIFIER = "given:contains";
    static final String MODIFIER_CONTAINS = "contains";
    static final String PARAMETER_GIVEN = "given";
    static final String PARAMETER_TEST_TEST = "Test:test";
    static final String PARAMETER_MODIFIER_CONTAINS = ParameterModifier.CONTAINS.getName();
    static final FhirRequestParameter PARAM_GIVEN_DESCRIPTION = new FhirRequestParameter(PARAMETER_GIVEN, ParameterType.STRING, true, null);
    static final FhirRequestParameter PARAM_GIVEN_TEST_DESCRIPTION = new FhirRequestParameter(PARAMETER_TEST_TEST, ParameterType.STRING, false, null);
    static final String PARAMETER_GIVEN_VALUE = "rob";
    static final String PARAMETER_DATE_VALUE = "2018-08-28";
    static final String PARAMETER_DATE_NAME = "date";
    static final String PARAMETER_NUMBER_NAME = "number";
    static final FhirRequestParameter PARAM_DATE_DESCRIPTION = new FhirRequestParameter(PARAMETER_DATE_NAME, ParameterType.DATE, false, "[0-9]{4}(-(0[1-9]|1[0-2])(-(0[0-9]|[1-2][0-9]|3[0-1]))?)?");
    static final FhirRequestParameter PARAM_NUMBER_DESCRIPTION = new FhirRequestParameter(PARAMETER_NUMBER_NAME, ParameterType.NUMBER, false, null);
    static final String PARAMETER_NOT_SPECIFIED = "birthplace";
    static final String PARAMETER_VALUE_FLOAT = "78987.098";
    static final String PARAMETER_VALUE_NEGATIVE = "-98";
    static final String UNKNOWN_MODIFIER = "start-with";
    static final String INCOMPLETE_PARAMETER = "parameter=";
    static final String NUMBER_WITH_CONTAINS = "number:contains";
    static final String RETRIEVE_PATIENT_REQUEST = "http://localhost:8080/Patient/UI9879098";
    static final String TOO_MANY_QUESTION_MARKS_REQUEST = "http://localhost:8080/Patient?id=UI9879098?given=rob";
    static final String VALID_DOMAIN = "ehealthsuisse.ihe-europe.net:10443";
    static final String LOCALHOST = "localhost";
    static final String INVALID_DOMAIN = "an_invalid_domain:4:34";
    static final String URL_NO_PROTOCOL = "gazelle.ihe.net/PatientManager/fhir/Patient/$ihe-pix";
    static final String HTTP_URL = "http://" + URL_NO_PROTOCOL;
    static final String HTTPS_URL = "https://" + URL_NO_PROTOCOL;
    static final String URL_PQDM_READ = "http://ehealthsuisse.ihe-europe.net/fhir/Patient/789";
    static final String URL_INVALID_RESOURCE = "http://ehealthsuisse.ihe-europe.net/fhir/AuditEvent";
    static final String URL_WRONG_OPERATION = "http://gazelle.ihe.net/PatientManager/fhir/Patient/$ihe-pdqm";
    static final String BASE_URL_PIXM = "https://gazelle.ihe.net/PatientManager/fhir/Patient/$ihe-pix";
    static final String TOKEN_SYSTEM_ONLY = "urn:ietf:rfc:3986|";
    static final String TOKEN_SYSTEM_CODE = "http://fhir.org/value-test|8888";
    static final String TOKEN_CODE_ONLY = "890987";
    static final String TOKEN_CODE_NO_SYSTEM = "|alphanumericcode";
    static final String TOKEN_INVALID_SYSTEM = "notasystem|code";
    static final String TOKEN_CODE_URI = "urn:ietf:rfc:3986|urn:uuid:6b7ce813-7548-4906-8a8e-7210f7f2ac5c";
    static final String DOCUMENT_REFERENCE_WITH_DATE = "https://test.org/base/DocumentReference?type=57830-2&_lastUpdated=gt2024-10-15T13:28:17.239+12:00&_elements=id";

    public static FhirURLValidatorDescription getValidatorDescriptionForTest() {
        FhirURLValidatorDescription validatorDescription = new FhirURLValidatorDescription();
        validatorDescription.setResourceName("Patient");
        validatorDescription.setOperation(PIXM_OPERATION);
        List<FhirRequestParameter> parameters = validatorDescription.getRequestParameters();
        parameters.add(PARAM_GIVEN_DESCRIPTION);
        parameters.add(PARAM_GIVEN_TEST_DESCRIPTION);
        parameters.add(PARAM_DATE_DESCRIPTION);
        parameters.add(PARAM_NUMBER_DESCRIPTION);
        return validatorDescription;
    }

    public static List<String> getUsedParameterCompleteList(){
        List<String> parameters = new ArrayList<String>();
        parameters.add(PARAMETER_GIVEN);
        return parameters;
    }

    public static List<String> getUsedParameterMissingRequiredList(){
        List<String> parameters = new ArrayList<String>();
        parameters.add(PARAMETER_NUMBER_NAME);
        return parameters;
    }
}