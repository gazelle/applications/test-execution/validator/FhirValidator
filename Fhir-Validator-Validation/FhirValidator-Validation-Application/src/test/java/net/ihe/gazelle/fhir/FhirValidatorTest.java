package net.ihe.gazelle.fhir;

import net.ihe.gazelle.fhir.validator.common.business.FhirValidatorDescription;
import net.ihe.gazelle.fhir.validator.validation.application.GazelleFhirValidator;
import net.ihe.gazelle.validation.MDAValidation;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * <p>FhirValidatorTest class.</p>
 *
 * @author abe
 * @version 1.0: 15/12/17
 */

public class FhirValidatorTest extends TestUtils {

    private static final Logger LOG = LoggerFactory.getLogger(FhirValidatorTest.class);

    @Override
    protected MDAValidation validate(String message) {
        return null;
    }

    private void validateResource(String filepath){
        try {
            String message = readFile(filepath);
            FhirValidatorDescription basicFhirValidatorXml = getFhirResourceXml();
            GazelleFhirValidator validator = new GazelleFhirValidator(basicFhirValidatorXml);
            String result = validator.validateFhirMessage(message);
            System.out.println(result);
            Assert.assertNotNull(result);
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void validateAppoitment() {
        String filepath = "src/test/resources/fhirresources/Appointment.xml";
        validateResource(filepath);
    }

    @Test
    public void validateAuditEvent() {
        String filepath = "src/test/resources/fhirresources/AuditEvent.xml";
        validateResource(filepath);
    }

    @Test
    public void validateCommunicationRequest() {
        String filepath = "src/test/resources/fhirresources/CommunicationRequest.xml";
        validateResource(filepath);
    }
    @Test
    public void validateDiagnosticReport() {
        String filepath = "src/test/resources/fhirresources/DiagnosticReport.xml";
        validateResource(filepath);
    }

    @Test
    public void validateDocumentReference() {
        String filepath = "src/test/resources/fhirresources/DocumentReference.xml";
        validateResource(filepath);
    }

    @Test
    public void validateBundle() {
        String filepath = "src/test/resources/fhirresources/Bundle.xml";
        validateResource(filepath);
    }

    @Test
    public void validateEncounter() {
        String filepath = "src/test/resources/fhirresources/Encounter.xml";
        validateResource(filepath);
    }

    @Test
    public void validateObservation() {
        String filepath = "src/test/resources/fhirresources/Observation.xml";
        validateResource(filepath);
    }

    @Test
    public void validateOrganization() {
        String filepath = "src/test/resources/fhirresources/Organization.xml";
        validateResource(filepath);
    }

    @Test
    public void validatePractitioner() {
        String filepath = "src/test/resources/fhirresources/Practitioner.xml";
        validateResource(filepath);
    }

    @Test
    public void validateBinary() {
        String filepath = "src/test/resources/fhirresources/Binary.xml";
        validateResource(filepath);
    }

    @Test
    public void validatePatient() {
        String filepath = "src/test/resources/fhirresources/Patient.xml";
        validateResource(filepath);
    }




}
