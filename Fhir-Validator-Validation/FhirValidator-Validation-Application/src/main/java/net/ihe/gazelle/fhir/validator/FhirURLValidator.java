package net.ihe.gazelle.fhir.validator;

import net.ihe.gazelle.fhir.validator.common.business.*;
import net.ihe.gazelle.fhir.validator.validation.application.ReportCreator;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * <p>FhirRequestValidator class.</p>
 *
 * @author abe
 * @version 1.0: 27/08/18
 */

public class FhirURLValidator {


    private FhirURLValidatorDescription validatorDescription;
    private List<String> usedParameters;
    private MDAValidation validationResult;
    private int errorCount = 0;
    private int noteCount = 0;

    public FhirURLValidator() {
        usedParameters = new ArrayList<String>();
        validationResult = new MDAValidation();
    }

    private void addError(Notification error) {
        validationResult.getWarningOrErrorOrNote().add(error);
        errorCount++;
    }

    private void addNote(Notification note) {
        validationResult.getWarningOrErrorOrNote().add(note);
        noteCount++;
    }

    public MDAValidation validateRequest(String urlToValidate, FhirURLValidatorDescription validatorDescription) {
        this.validatorDescription = validatorDescription;
        if (urlToValidate == null || urlToValidate.isEmpty()) {
            Notification error = new Error();
            error.setDescription("The URL to validate is empty");
            addError(error);
        } else {
            // split using ? (separates base URL from URL parameters)
            String[] urlParts = splitStringAndGetParts(urlToValidate, "\\?");
            if (urlParts.length == 2) {
                validateBaseUrl(urlParts[0]);
                validateUrlParameters(urlParts[1]);
            } else if (urlParts.length == 1) {
                validateBaseUrl(urlParts[0]);
            } else {
                // FIXME review wording of the error
                Notification error = new Error();
                error.setDescription("? is a reserved character");
                addError(error);
            }
        }
        addValidationCounters();
        String validationStatus = errorCount == 0 ? ReportCreator.PASSED : ReportCreator.FAILED;
        validationResult.setResult(validationStatus);
        return validationResult;
    }

    private void addValidationCounters() {
        ValidationCounters counters = new ValidationCounters();
        counters.setNrOfValidationWarnings(BigInteger.ZERO);
        counters.setNrOfValidationInfos(BigInteger.ZERO);
        counters.setNrOfValidationNotes(BigInteger.valueOf(noteCount));
        counters.setNrOfValidationErrors(BigInteger.valueOf(errorCount));
        counters.setNrOfChecks(BigInteger.valueOf(noteCount + errorCount));
        validationResult.setValidationCounters(counters);
    }

    public String[] splitStringAndGetParts(String inString, String delimiter) {
        return inString.split(delimiter);
    }

    public void validateUrlParameters(String parametersFromUrl) {
        String[] parameters = splitStringAndGetParts(parametersFromUrl, "&"); // we shall not match "\&" here
        for (String parameter : parameters) {
            validateUrlParameter(parameter);
        }
        checkAllRequiredParametersArePresent();
    }

    public void checkAllRequiredParametersArePresent() {
        List<FhirRequestParameter> requiredParameters = getRequiredParametersForRequest();
        for (FhirRequestParameter requiredParam : requiredParameters) {
            final String name = requiredParam.getNameWithPrefix();
            if (usedParameters.contains(name)) {
                Notification note = new Note();
                note.setDescription(requiredParam.getName() + " is required and present");
                addNote(note);
            } else {
                Notification error = new Error();
                if (requiredParam.getPrefix() == null) {
                    error.setDescription(requiredParam.getName() + " is required but missing in this URL");
                } else {
                    error.setDescription("A parameter named " + requiredParam.getName() + " with a value prefixed by " + requiredParam.getPrefix().getPrefixValue()
                            + " is required but missing in this URL");
                }
                addError(error);
            }
        }
    }

    private List<FhirRequestParameter> getRequiredParametersForRequest() {
        List<FhirRequestParameter> allParameters = validatorDescription.getRequestParameters();
        List<FhirRequestParameter> requiredParameters = new ArrayList<FhirRequestParameter>();
        for (FhirRequestParameter parameter : allParameters) {
            if (parameter.isRequired()) {
                requiredParameters.add(parameter);
            }
        }
        return requiredParameters;
    }

    public void validateUrlParameter(String parameter) {
        String[] parameterParts = splitStringAndGetParts(parameter, "=");
        if (parameterParts.length != 2) {
            // FIXME review wording
            Notification error = new Error();
            error.setDescription("A parameter shall be made of a couple name/value");
            error.setLocation(parameter);
            addError(error);
        } else {
            IFhirRequestParameter testedParameter = validateParameterKey(parameterParts[0]);
            if (testedParameter != null) {
                validateParameterValue(parameterParts[1], testedParameter);
            }
        }
    }

    protected void validateParameterValue(String parameterPart, IFhirRequestParameter testedParameter) {
        // first split the repetitions (used for OR clauses)
        String[] repetitions = splitStringAndGetParts(parameterPart, ","); // we shall not match "\," here
        String prefix;
        String key = null;
        for (String value : repetitions) {
            prefix = getPrefix(value, testedParameter);
            if (prefix != null) {
                // value has to be truncated to remove the prefix
                value = value.substring(2);
                key = testedParameter.getName() + ":" + prefix;
            } else {
                key = testedParameter.getName();
            }
            validateParameterValueFormat(value, testedParameter);
        }
        usedParameters.add(key);
    }

    public void validateParameterValueFormat(String value, IFhirRequestParameter testedParameter) {
        String regex = testedParameter.getRegex();
        if (regex == null || regex.isEmpty()) {
            regex = testedParameter.getType().getDefaultRegex();
        }
        final Pattern pattern = Pattern.compile(regex);
        if (pattern.matcher(value).matches()) {
            Notification note = new Note();
            note.setDescription("Value for parameter " + testedParameter.getName() + " is correctly formatted");
            note.setLocation(value);
            addNote(note);
        } else {
            Notification error = new Error();
            error.setDescription("Value for parameter " + testedParameter.getName() + " shall match regex " + regex);
            error.setLocation(value);
            addError(error);
        }
    }

    private String getPrefix(String value, IFhirRequestParameter testedParameter) {
        // for number, quantity and dates, check if a prefix is used
        if (testedParameter.getType().isSupportsPrefix()) {
            FhirParameterPrefix prefix = FhirParameterPrefix.extractPrefixFromValue(value);
            if (prefix != null) {
                Notification note = new Note();
                note.setDescription("A prefix is used for parameter " + testedParameter.getName());
                note.setLocation(value);
                addNote(note);
                return prefix.getPrefixValue();
            }
        }
        return null;
    }

    public IFhirRequestParameter getParameterIfAllowed(String keyName) {
        List<FhirRequestParameter> allowedParameters = validatorDescription.getRequestParameters();
        IFhirRequestParameter foundParameter = CommonFhirRequestParameter.getParameterByName(keyName);
        if (foundParameter == null) {
            for (FhirRequestParameter parameter : allowedParameters) {
                if (parameter.getName().equals(keyName)) {
                    foundParameter = parameter;
                }
            }
        }
        if (foundParameter != null) {
            Notification note = new Note();
            note.setDescription(keyName + " parameter is present");
            addNote(note);
            return foundParameter;
        } else {
            Notification error = new Error();
            error.setDescription(keyName + " is not an allowed parameter");
            addError(error);
            return null;
        }
    }


    public IFhirRequestParameter validateParameterKey(String key) {

        IFhirRequestParameter parameterDefinition = getParameterIfAllowed(key);
        if (parameterDefinition != null) {
            return parameterDefinition;
        }

        String[] keyParts = splitStringAndGetParts(key, ":");
        if (keyParts.length > 2) {
            Notification error = new Error();
            error.setDescription(key + " is not allowed for parameters : too many parts");
            error.setLocation(key);
            addError(error);
            return null;
        } else {
            String keyName = keyParts[0];
            IFhirRequestParameter splittedParameterDefinition = getParameterIfAllowed(keyName);
            if (splittedParameterDefinition != null) {
                this.validationResult.getWarningOrErrorOrNote().remove(this.validationResult.getWarningOrErrorOrNote().size() - 2);
                this.errorCount = this.errorCount - 1;

                String usedModifier = keyParts[1];
                ParameterModifier splittedParameterModifier = getModifierByName(usedModifier, key);
                if (splittedParameterModifier != null) {
                    if (splittedParameterDefinition.getType().allowsModifier(splittedParameterModifier)) {
                        Notification note = new Note();
                        note.setDescription("Modifier " + usedModifier + " is used for parameter " + keyName);
                        note.setLocation(key);
                        addNote(note);
                    } else {
                        Notification error = new Error();
                        error.setDescription(usedModifier + " is not allowed for parameters of type " + splittedParameterDefinition.getType().getName());
                        error.setLocation(key);
                        addError(error);
                    }
                }
            }
            return splittedParameterDefinition;
        }
    }


    public ParameterModifier getModifierByName(String usedModifier, String key) {
        ParameterModifier parameterModifier = ParameterModifier.getModifierByName(usedModifier);
        if (parameterModifier == null) {
            Notification error = new Error();
            error.setDescription(usedModifier + " is an unknown modifier");
            error.setLocation(key);
            addError(error);
        }
        return parameterModifier;
    }

    public void validateBaseUrl(String baseUrl) {
        // check protocol and start parsing after protocol declaration
        String baseUrlNoProtocol = checkProtocolAndReturnUrl(baseUrl);
        if (baseUrlNoProtocol != null) {
            // check URL format
            String[] parts = baseUrlNoProtocol.split("/");
            int index = 0;
            boolean resourceIsPresent = false;
            for (String part : parts) {
                resourceIsPresent = validateBaseUrlParts(part, index);
                if (resourceIsPresent) {
                    break;
                }
                index++;
            }
            index++;
            if (index < parts.length) {
                // that means that we found the resource name
                String nextPart = parts[index];
                if (nextPart.startsWith("$")) {
                    // an operation is called
                    checkOperation(nextPart);
                } else if (validatorDescription.getOperation() != null && !validatorDescription.getOperation().isEmpty()) {
                    // an operation is expected but not present
                    Notification error = new Error();
                    error.setDescription("Operation " + validatorDescription.getOperation() + " is expected but missing");
                    error.setLocation(nextPart);
                    addError(error);
                } else {
                    // it's not an operation, it is the identifier of the resource
                    Pattern pattern = Pattern.compile(ValidatorConstants.ID_REGEX);
                    if (pattern.matcher(nextPart).matches()) {
                        Notification note = new Note();
                        note.setDescription("The resource's logical id is correctly formatted");
                        note.setLocation(nextPart);
                        addNote(note);
                    } else {
                        Notification error = new Error();
                        error.setDescription("The resource's logical id shall match regex " + ValidatorConstants.ID_REGEX);
                        error.setLocation(nextPart);
                        addError(error);
                    }
                }
                index++;
                if (index < parts.length) {
                    nextPart = parts[index];
                    checkOperation(nextPart);
                }
            } else if (!resourceIsPresent) {
                Notification error = new Error();
                error.setDescription("Declaration of the resource is missing, expecting " + validatorDescription.getResourceName());
                addError(error);
            }
        }
    }

    /**
     * @param part
     * @param index
     * @return true if we reach the resourceName part of the URL
     */
    public boolean validateBaseUrlParts(String part, int index) {
        if (index == 0) {
            validateDomain(part);
        } else if (part.equals(validatorDescription.getResourceName())) {
            Notification note = new Note();
            note.setDescription("Query is performed on resource with name: " + part);
            addNote(note);
            return true;
        } else {
            Pattern pattern = Pattern.compile(ValidatorConstants.URL_PART_PATTERN);
            if (!pattern.matcher(part).matches()) {
                Notification error = new Error();
                error.setDescription("The URL is not valid");
                error.setLocation(part);
                addError(error);
            }
        }
        return false;
    }

    public void validateDomain(String part) {
        Pattern domainPattern = Pattern.compile(ValidatorConstants.DOMAIN);
        Notification notification;
        if (domainPattern.matcher(part).matches()) {
            notification = new Note();
            notification.setDescription("A valid domain is used");
            notification.setLocation(part);
            addNote(notification);
        } else {
            notification = new Error();
            notification.setDescription("This is not a valid domain name");
            notification.setLocation(part);
            addError(notification);
        }
    }

    public void checkOperation(String nextPart) {
        if (validatorDescription.isOperationDefined()) {
            String operation = "$" + validatorDescription.getOperation();
            if (operation.equals(nextPart)) {
                Notification note = new Note();
                note.setDescription("Operation " + operation + " is used");
                addNote(note);
            } else {
                Notification error = new Error();
                error.setDescription("Operation " + operation + " is expected but missing in the request");
                addError(error);
            }
        } else {
// TODO
        }
    }

    public String checkProtocolAndReturnUrl(String baseUrl) {
        String baseUrlNoProtocol = null;
        if (baseUrl.startsWith(ValidatorConstants.HTTP)) {
            baseUrlNoProtocol = baseUrl.substring(ValidatorConstants.HTTP.length());
            Notification note = new Note();
            note.setDescription("HTTP protocol is used");
            addNote(note);
        } else if (baseUrl.startsWith(ValidatorConstants.HTTPS)) {
            baseUrlNoProtocol = baseUrl.substring(ValidatorConstants.HTTPS.length());
            Notification note = new Note();
            note.setDescription("HTTPS protocol is used");
            addNote(note);
        } else {
            Notification error = new Error();
            error.setDescription("Either HTTP or HTTPS protocols shall be used");
            error.setLocation(baseUrl);
            addError(error);
        }
        return baseUrlNoProtocol;
    }

    public List<Object> getNotifications() {
        return validationResult.getWarningOrErrorOrNote();
    }

    public void setUsedParameters(List<String> usedParameters) {
        this.usedParameters = usedParameters;
    }

    public void setValidatorDescription(FhirURLValidatorDescription validatorDescription) {
        this.validatorDescription = validatorDescription;
    }

    public FhirURLValidatorDescription getValidatorDescription() {
        return validatorDescription;
    }

    public boolean isValidationPassed() {
        return errorCount == 0;
    }
}