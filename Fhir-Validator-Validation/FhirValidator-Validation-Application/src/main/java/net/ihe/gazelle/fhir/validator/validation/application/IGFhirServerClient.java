package net.ihe.gazelle.fhir.validator.validation.application;

import ca.uhn.fhir.rest.api.EncodingEnum;
import net.ihe.gazelle.validation.DetailedResult;

public interface IGFhirServerClient {

    /**
     * Send message to IG Fhir Server.
     *
     * @param messageToValidate literal content of the message to validate.
     * @param profile           literal value of the profile URL.
     * @param format            format of the resource to validate.
     * @param result            validation report to fill with validation outcome.
     * @return the filled validation report.
     */
    DetailedResult sendMessageForValidation(String messageToValidate, String profile, EncodingEnum format, DetailedResult result);
}
