package net.ihe.gazelle.fhir.validator.validation.application.operationoutcome;

import ca.uhn.fhir.rest.api.EncodingEnum;
import net.ihe.gazelle.fhir.validator.common.peripherals.hl7Fhir.OperationOutcomeAdapter;
import net.ihe.gazelle.fhir.validator.common.business.Exception.FhirValidatorException;
import net.ihe.gazelle.fhir.validator.validation.application.ValidatorCommon;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

/**
 * <p>OperationOutcomeValidator class.</p>
 *
 * @author aberge
 * @version 1.0: 23/11/17
 */

public class OperationOutcomeValidator extends ValidatorCommon {

    private OperationOutcomeAdapter operationOutcomeAdapter ;

    public OperationOutcomeValidator(String message, EncodingEnum format) {
        super(message, format);
    }

    public MDAValidation validateOperationOutcome() {
        isOperationOutcome();
        if (operationOutcomeAdapter != null) {
            validateIsOperationOutcomeSeverityError(operationOutcomeAdapter);
            validateIsDiagnosticsPresent();
        }
        MDAValidation validationResult = buildReport();
        return validationResult;
    }

    private void validateIsDiagnosticsPresent() {
        Notification notification;
        if (operationOutcomeAdapter.isDiagnosticElementValid()) {
            notification = new Note();
        } else {
            notification = new Info();
        }
        fillNotification(null, OperationOutcomeAssertions.CONSTRAINT_DIAGNOSTICS, notification);
        notifications.add(notification);
    }


    private void validateIsOperationOutcomeSeverityError(OperationOutcomeAdapter outcome) {
        Notification notification;
        if (outcome.checkSeverityIsError()) {
            notification = new Note();
        } else {
            notification = new Error();
        }
        fillNotification(null, OperationOutcomeAssertions.CONSTRAINT_SEVERITY_ERROR, notification);
        notifications.add(notification);
    }

    private void isOperationOutcome() {
        Notification notification;
        try {
            operationOutcomeAdapter = new OperationOutcomeAdapter(message, format.getFormatContentType());
            notification = new Note();
        } catch (FhirValidatorException e) {
            operationOutcomeAdapter = null;
            notification = new Error();
        }
        fillNotification(null, OperationOutcomeAssertions.CONSTRAINT_ROOT_ELEMENT, notification);
        notifications.add(notification);
    }
}
