package net.ihe.gazelle.fhir.validator.validation.application;

import net.ihe.gazelle.fhir.validator.common.business.FhirValidatorDescription;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.*;
import net.ihe.version.ws.Interface.Preferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>ReportCreator class.</p>
 *
 * @author aberge
 * @version 1.0: 16/11/17
 */

public class ReportCreator {

    public static final String PASSED = "PASSED";
    public static final String FAILED = "FAILED";
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportCreator.class);
    public static final String ABORTED = "ABORTED";
    public static final String SKIPPED = "SKIPPED";

    public static ValidationResultsOverview fhirValidationOverview(boolean passed, FhirValidatorDescription validatorNames) {
        ValidationResultsOverview validationResultsOverview = new ValidationResultsOverview();
        Date currentDate = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy, MM dd");
        DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
        validationResultsOverview.setValidationDate(dateFormat.format(currentDate));
        validationResultsOverview.setValidationTime(timeFormat.format(currentDate));
        validationResultsOverview.setValidationServiceName("Gazelle FHIR Validator");
        validationResultsOverview.setValidationEngine(validatorNames.getName());
        validationResultsOverview.setValidationServiceVersion(Preferences.getProperty("buildVersion"));

        String status = passed ? PASSED:FAILED;
        validationResultsOverview.setValidationTestResult(status);
        return validationResultsOverview;
    }

    public static String getDetailedResultAsString(DetailedResult detailedResult) {
        if (detailedResult != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.validation");
                Marshaller m = jc.createMarshaller();
                m.setProperty(Marshaller.JAXB_ENCODING, StandardCharsets.UTF_8.name());
                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                m.marshal(detailedResult, baos);
            } catch (JAXBException e) {
                LOGGER.error(e.getMessage(), e);
            }
            try {
                return baos.toString(StandardCharsets.UTF_8.name());
            } catch (UnsupportedEncodingException e) {
                LOGGER.warn("UTF-8 encoding is not supported, relying on default encoding: " + Charset.defaultCharset().displayName(), e);
                return null;
            }
        }
        return null;
    }

    public static DetailedResult buildReportForAbortedValidation(FhirValidatorDescription validator, IOException e) {
        DetailedResult detailedResult = new DetailedResult();
        ValidationResultsOverview overview = fhirValidationOverview(false, validator);
        overview.setValidationTestResult(ABORTED);
        detailedResult.setValidationResultsOverview(overview);
        MDAValidation mdaValidation = new MDAValidation();
        mdaValidation.setResult(SKIPPED);
        Notification notification = new Error();
        notification.setLocation("message");
        notification.setDescription(e.getMessage());
        notification.setIdentifiant("Pre-processing step");
        mdaValidation.getWarningOrErrorOrNote().add(notification);
        detailedResult.setMDAValidation(mdaValidation);
        return detailedResult;
    }
}
