package net.ihe.gazelle.fhir.validator.validation.application;

import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.gazelle.xmltools.xsd.ValidationException;
import net.ihe.gazelle.xmltools.xsd.XSDValidator;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class XMLValidation {

	private static Logger log = LoggerFactory.getLogger(XMLValidation.class);

	private static final SAXParserFactory factoryBASIC;

	static {
		factoryBASIC = SAXParserFactory.newInstance();
		try {
			factoryBASIC.setValidating(false);
			factoryBASIC.setNamespaceAware(true);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * Parses the file using SAX to check that it is a well-formed XML file
	 * 
	 * @param string
	 * @return
	 */
	public DocumentWellFormed isXMLWellFormed(String string) {
		return validateIfDocumentWellFormedXML(string, "");
	}

	private DocumentWellFormed validateIfDocumentWellFormedXML(
			String message, String xsdpath) {
		DocumentWellFormed documentWellFormedResult = new DocumentWellFormed();
		return validXMLUsingXSD(message, xsdpath, factoryBASIC, documentWellFormedResult);
	}

	private static <T extends DocumentValidXSD> T validXMLUsingXSD(
			String message, String xsdpath, SAXParserFactory factory,
			T documentValidResult) {
		List<ValidationException> exceptions = new ArrayList<ValidationException>();
		try {
			ByteArrayInputStream bais = new ByteArrayInputStream(
					message.getBytes(StandardCharsets.UTF_8));
			XSDValidator validator = new XSDValidator();
			exceptions = validator.validateUsingFactoryAndSchema(bais,
					xsdpath, factory);
		} catch (Exception e) {
			exceptions.add(handleException(e));
		}
		return extractValidationResult(exceptions, documentValidResult);
	}

	private static <T extends DocumentValidXSD> T extractValidationResult(
			List<ValidationException> exceptions, T dv) {

		dv.setResult("PASSED");

		if (exceptions == null || exceptions.size() == 0) {
			dv.setNbOfErrors("0");
			dv.setResult("PASSED");
			return dv;
		} else {
			Integer nbOfErrors = 0;
			Integer nbOfWarnings = 0;
			for (ValidationException ve : exceptions) {
				if (ve.getSeverity() == null) {
					ve.setSeverity("error");
				}
				if ((ve.getSeverity() != null)
						&& (ve.getSeverity().equals("warning"))) {
					nbOfWarnings++;
				} else {
					nbOfErrors++;
				}
				XSDMessage xsd = new XSDMessage();
				xsd.setMessage(ve.getMessage());
				xsd.setSeverity(ve.getSeverity());
				if (StringUtils.isNumeric(ve.getLineNumber())) {
					xsd.setLineNumber(Integer.valueOf(ve.getLineNumber()));
				}
				if (StringUtils.isNumeric(ve.getColumnNumber())) {
					xsd.setColumnNumber(Integer.valueOf(ve.getColumnNumber()));
				}
				dv.getXSDMessage().add(xsd);
			}
			dv.setNbOfErrors(nbOfErrors.toString());
			dv.setNbOfWarnings(nbOfWarnings.toString());
			if (nbOfErrors > 0) {
				dv.setResult("FAILED");
			}
			return dv;
		}

	}

	private static ValidationException handleException(Exception e) {
		ValidationException ve = new ValidationException();
		ve.setLineNumber("0");
		ve.setColumnNumber("0");
		if (e != null && e.getMessage() != null) {
			ve.setMessage("error on validating : " + e.getMessage());
		} else if (e != null && e.getCause() != null
				&& e.getCause().getMessage() != null) {
			ve.setMessage("error on validating : " + e.getCause().getMessage());
		} else if (e != null) {
			ve.setMessage("error on validating. The exception generated is of kind : "
					+ e.getClass().getSimpleName());
		} else {
			ve.setMessage("Unexpected exception raised during the validation");
		}
		ve.setSeverity("error");
		return ve;
	}

}
