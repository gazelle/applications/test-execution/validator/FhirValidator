package net.ihe.gazelle.fhir.validator.validation.application;

import ca.uhn.fhir.validation.FhirValidator;
import ca.uhn.fhir.validation.IValidatorModule;
import ca.uhn.fhir.validation.SchemaBaseValidator;
import ca.uhn.fhir.validation.schematron.SchematronBaseValidator;
import net.ihe.gazelle.fhir.context.FhirContextProvider;
import net.ihe.gazelle.fhir.validator.common.business.FhirResourceValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.peripherals.ValidationSupportProvider;
import net.ihe.gazelle.fhir.validator.common.peripherals.hl7Fhir.FhirInstanceValidatorAdapter;
import net.ihe.gazelle.fhir.validator.common.peripherals.hl7Fhir.ValidationSupportChainAdapter;
import org.hl7.fhir.r5.utils.IResourceValidator;

/**
 * <p>FhirValidatorProvider class.</p>
 *
 * @author aberge
 * @version 1.0: 23/11/17
 */

public final class FhirValidatorProvider {

    /**
     * Private default constructor for the class
     */
    private FhirValidatorProvider() {
        //empty
    }

    private static FhirValidator fhirValidator;
    private static FhirValidator noSchematronFhirValidator;

    public static synchronized FhirValidator instanceWithSchematron() {
        if (fhirValidator == null) {
            fhirValidator = createValidatorWithSchematron();
        }
        return fhirValidator;
    }

    private static FhirValidator createValidatorWithSchematron() {
        FhirValidator withSchematronValidator = FhirContextProvider.instance().newValidator();
        IValidatorModule module1 = createSchemaBaseValidatorModule();
        IValidatorModule module2 = createSchematronBasedValidatorModule();
        withSchematronValidator.registerValidatorModule(module1);
        withSchematronValidator.registerValidatorModule(module2);
        withSchematronValidator.setValidateAgainstStandardSchema(true);
        withSchematronValidator.setValidateAgainstStandardSchematron(true);
        return withSchematronValidator;
    }

    private static IValidatorModule createSchematronBasedValidatorModule() {
        return new SchematronBaseValidator(FhirContextProvider.instance());
    }

    private static IValidatorModule createSchemaBaseValidatorModule() {
        return new SchemaBaseValidator(FhirContextProvider.instance());
    }

    public static synchronized FhirValidator instanceWithoutSchematron() {
        if (noSchematronFhirValidator == null) {
            noSchematronFhirValidator = FhirContextProvider.instance().newValidator();
            IValidatorModule module1 = createSchemaBaseValidatorModule();
            noSchematronFhirValidator.registerValidatorModule(module1);
            noSchematronFhirValidator.setValidateAgainstStandardSchema(true);
            noSchematronFhirValidator.setValidateAgainstStandardSchematron(false);
        }
        return noSchematronFhirValidator;
    }

    public static FhirValidator instanceForCustomStructureDefinition(FhirResourceValidatorDescription validator) {
        FhirValidator customStructureDefValidator = FhirContextProvider.instance().newValidator();
        FhirInstanceValidatorAdapter fhirInstanceValidatorAdapter = configureFhirInstanceValidator(validator);
        customStructureDefValidator.registerValidatorModule(fhirInstanceValidatorAdapter);
        return customStructureDefValidator;
    }

    private static FhirInstanceValidatorAdapter configureFhirInstanceValidator(FhirResourceValidatorDescription validator) {
        ValidationSupportChainAdapter support = ValidationSupportProvider.getValidationSupportChain(validator);
        FhirInstanceValidatorAdapter fhirInstanceValidatorAdapter = new FhirInstanceValidatorAdapter(FhirContextProvider.instance());
        fhirInstanceValidatorAdapter.setAnyExtensionsAllowed(true);
        fhirInstanceValidatorAdapter.setValidationSupport(support);
        fhirInstanceValidatorAdapter.setBestPracticeWarningLevel(IResourceValidator.BestPracticeWarningLevel.Warning);

        return fhirInstanceValidatorAdapter;
    }
}
