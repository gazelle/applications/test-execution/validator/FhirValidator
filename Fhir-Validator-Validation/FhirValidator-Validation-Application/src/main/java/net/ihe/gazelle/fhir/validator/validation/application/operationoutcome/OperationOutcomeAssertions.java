package net.ihe.gazelle.fhir.validator.validation.application.operationoutcome;

import net.ihe.gazelle.fhir.validator.common.business.FhirAssertion;
import net.ihe.gazelle.validation.Assertion;

import java.util.Arrays;
import java.util.List;

/**
 * <p>OperationOutcomeAssertions enum.</p>
 *
 * @author aberge
 * @version 1.0: 24/11/17
 */
public enum OperationOutcomeAssertions implements FhirAssertion {

    CONSTRAINT_ROOT_ELEMENT("constraint_operationoutcome_resourceName",
            "The root element shall be an OperationOutcome",
            "/",
            new Assertion("OO", "OO-002")),
    CONSTRAINT_SEVERITY_ERROR("constraint_operationoutcome_severity_error",
            "The operationOutcome shall report an error",
            "/OperationOutcome/severity",
            new Assertion("OO", "OO-003")),
    CONSTRAINT_DIAGNOSTICS("constraint_operationoutcome_diagnostics_present",
            "The detailed reason of the failure may be reported in a diagnostics element",
            "/OperationOutcome",
            new Assertion("OO", "OO-001"));


    String description;
    String identifier;
    String location;
    Assertion[] testedAssertion;

    OperationOutcomeAssertions(String inIdentifier, String inDescription, String inLocation, Assertion... inAssertion) {
        this.description = inDescription;
        this.identifier = inIdentifier;
        this.location = inLocation;
        this.testedAssertion = inAssertion;
    }

    public String getDescription() {
        return description;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getLocation() {
        return location;
    }

    public List<Assertion> getTestedAssertion() {
        return Arrays.asList(testedAssertion);
    }
}

