/*
 * Copyright 2012 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.fhir.validator.validation.peripherals.ws;

import net.ihe.gazelle.fhir.validator.common.adapter.FhirResourceValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.adapter.FhirURLValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.adapter.dao.FhirValidatorDescriptionDAO;
import net.ihe.gazelle.fhir.validator.common.business.FhirValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.peripherals.mapper.MapperAdaptor;
import net.ihe.gazelle.fhir.validator.validation.application.GazelleFhirValidator;
import net.ihe.gazelle.fhir.validator.validation.application.IGFhirServerClient;
import net.ihe.gazelle.preferences.ApplicationConfigurationDAO;
import net.ihe.gazelle.validator.validation.exception.GazelleValidationException;
import net.ihe.gazelle.validator.validation.model.ValidatorDescription;
import net.ihe.gazelle.validator.validation.ws.AbstractModelBasedValidation;
import net.ihe.version.ws.Interface.Preferences;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

/**
 * Validation Webservice for FHIR Validations.
 */
@Stateless
@WebService(name = "ModelBasedValidationWS",
        serviceName = "ModelBasedValidationWSService",
        portName = "ModelBasedValidationWSPort",
        targetNamespace = "http://ws.mb.validator.gazelle.ihe.net")
public class GazelleFhirValidationWS extends AbstractModelBasedValidation {
    private MapperAdaptor mapper = new MapperAdaptor();
    //Should inject
    private FhirValidatorDescriptionDAO dao = new FhirValidatorDescriptionDAO();
    @Inject
    private ApplicationConfigurationDAO applicationConfigurationDAO;

    /**
     * {@inheritDoc}
     */
    @Override
    @WebMethod
    @WebResult(name = "about")
    public String about() {
        String toolVersion = Preferences.getProperty("buildVersion");
        StringBuilder about = new StringBuilder();
        about.append("GazelleFhirValidator");
        if (toolVersion != null) {
            about.append(" (");
            about.append(toolVersion);
            about.append(")");
        }
        about.append(" is a validation service for FHIR message");
        return about.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String buildReportOnParsingFailure(GazelleValidationException e, ValidatorDescription validatorDescription) {
        // we don't need to parse received content
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String executeValidation(String messageAsString, ValidatorDescription validatorDescription, boolean extracted) throws GazelleValidationException {
        if (validatorDescription == null) {
            throw new GazelleValidationException("The validator is mandatory");
        } else if (messageAsString == null || messageAsString.isEmpty()) {
            throw new GazelleValidationException("The received message is empty");
        } else {
            FhirValidatorDescription validatorName;
            if (validatorDescription instanceof FhirResourceValidatorDescription) {
                validatorName = mapper.map(validatorDescription, net.ihe.gazelle.fhir.validator.common.business.FhirResourceValidatorDescription.class);
            } else if (validatorDescription instanceof FhirURLValidatorDescription) {
                validatorName = mapper.map(validatorDescription, net.ihe.gazelle.fhir.validator.common.business.FhirURLValidatorDescription.class);
            } else {
                throw new GazelleValidationException("Cannot find requested validator");
            }

            GazelleFhirValidator validator = new GazelleFhirValidator(validatorName);
            IGFhirServerClient igFhirServerClient = new IGFhirServerClientImpl(applicationConfigurationDAO.getValue("ig_fhir_server_url"));
            validator.setIgFhirServerClient(igFhirServerClient);
            String result = validator.validateFhirMessage(messageAsString);
            addValidatorUsage(validatorName.getName(), validator.getStatus());
            return result;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ValidatorDescription getValidatorByOidOrName(String s) {
        return dao.getValidatorByOidOrName(s);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ValidatorDescription> getValidatorsForDescriminator(String descriminator) {
        return dao.listValidatorsForDescriminator(descriminator);
    }

}