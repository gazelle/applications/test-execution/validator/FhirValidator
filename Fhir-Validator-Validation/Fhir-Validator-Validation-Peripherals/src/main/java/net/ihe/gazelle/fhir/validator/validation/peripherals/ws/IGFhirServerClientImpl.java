package net.ihe.gazelle.fhir.validator.validation.peripherals.ws;

import ca.uhn.fhir.rest.api.EncodingEnum;
import net.ihe.gazelle.fhir.validator.common.business.Exception.FhirValidatorException;
import net.ihe.gazelle.fhir.validator.common.peripherals.hapi.HapiParserAdapter;
import net.ihe.gazelle.fhir.validator.validation.application.IGFhirServerClient;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;


public class IGFhirServerClientImpl implements IGFhirServerClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(IGFhirServerClientImpl.class);
    private String serverUrl;

    /**
     * Default constructor for the class.
     *
     * @param serverUrl URL of the server.
     */
    public IGFhirServerClientImpl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DetailedResult sendMessageForValidation(String messageToValidate, String profile, EncodingEnum format, DetailedResult result) {
        String validationOutcome = null;
        try {
            validationOutcome = sendMessageToValidation(messageToValidate, profile, format.getResourceContentTypeNonLegacy());
        } catch (Exception e) {
            LOGGER.error("Unexpected exception sending message to IG Server !", e);
            result.setMDAValidation(mdaValidationForException("Unexpected exception sending message to IG Server", e));
        }
        if (validationOutcome != null) {
            OperationOutcome operationOutcome;
            try {
                operationOutcome = HapiParserAdapter.parseOperationOutcome(validationOutcome, format.getFormatContentType());
                result.setMDAValidation(mapOperationOutcomeToMDAValidation(operationOutcome));
            } catch (FhirValidatorException e) {
                LOGGER.error("Unable to parse IG Server response !", e);
                result.setMDAValidation(mdaValidationForException("Unable to parse IG Server response", e));
            }
        }
        return result;
    }

    /**
     * Send the message to the server.
     *
     * @param messageToValidate literal content of the message to validate.
     * @param profile           literal value of the url of the profile to validate against.
     * @param contentType       literal value of the Content-Type of the resource to validate.
     * @return the literal value of the response entity.
     */
    private String sendMessageToValidation(String messageToValidate, String profile, String contentType) {
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {

            HttpPost httppost = new HttpPost(serverUrl + "/$validate?profile=" +
                    URLEncoder.encode(profile, "UTF-8"));

            StringEntity stringEntity = new StringEntity(messageToValidate, StandardCharsets.UTF_8);
            httppost.setEntity(stringEntity);
            httppost.setHeader(new BasicHeader(HTTP.CONTENT_TYPE, contentType));

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                try (InputStream inputStream = entity.getContent()) {
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                    String result = reader.lines()
                            .collect(Collectors.joining("\n"));
                    reader.close();
                    return result;
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error contacting remote FHIR Server for validation.", e);
        }
        return null;
    }

    /**
     * Map an {@link OperationOutcome} object to an {@link MDAValidation} report.
     *
     * @param operationOutcome to map.
     * @return mapped {@link MDAValidation}
     */
    private MDAValidation mapOperationOutcomeToMDAValidation(OperationOutcome operationOutcome) {
        MDAValidation mdaValidationResult = new MDAValidation();

        List<OperationOutcome.OperationOutcomeIssueComponent> issues = operationOutcome.getIssue();

        int countErrorMessage = 0;
        int countWarningMessage = 0;
        int countInfoMessage = 0;

        for (OperationOutcome.OperationOutcomeIssueComponent issue : issues) {
            Notification notification;

            switch (issue.getSeverity()) {
                case ERROR:
                case FATAL:
                    notification = new Error();
                    notification.setTest(String.valueOf(countErrorMessage));
                    countErrorMessage++;
                    break;
                case WARNING:
                    notification = new Warning();
                    notification.setTest(String.valueOf(countWarningMessage));
                    countWarningMessage++;
                    break;
                default:
                    notification = new Info();
                    notification.setTest(String.valueOf(countInfoMessage));
                    countInfoMessage++;
                    break;
            }
            if (issue.getLocation() != null && !issue.getLocation().isEmpty()) {
                    notification.setLocation(convertFhirPathIntoXPath(issue.getLocation().get(0).getValue()));
            }
            notification.setDescription(issue.getDiagnostics());
            mdaValidationResult.getWarningOrErrorOrNote().add(notification);
        }
        ValidationCounters counters = new ValidationCounters();
        counters.setNrOfChecks(BigInteger.valueOf(issues.size()));
        counters.setNrOfValidationErrors(BigInteger.valueOf(countErrorMessage));
        counters.setNrOfValidationInfos(BigInteger.valueOf(countInfoMessage));
        counters.setNrOfValidationWarnings(BigInteger.valueOf(countWarningMessage));
        mdaValidationResult.setValidationCounters(counters);
        if (countErrorMessage == 0) {
            mdaValidationResult.setResult("PASSED");
        } else {
            mdaValidationResult.setResult("FAILED");
        }
        return mdaValidationResult;
    }

    /**
     * Create an {@link MDAValidation} report for an error that was raised during the validation process.
     *
     * @param e       exception to report
     * @param message message to display with the error
     * @return created {@link MDAValidation} report
     */
    private MDAValidation mdaValidationForException(String message, Exception e) {
        MDAValidation mdaValidationResult = new MDAValidation();

        mdaValidationResult.setResult("FAILED");

        Notification notification = new Error();
        notification.setDescription(getExceptionReportMessage(message, e));
        mdaValidationResult.getWarningOrErrorOrNote().add(notification);

        ValidationCounters counters = new ValidationCounters();
        counters.setNrOfChecks(BigInteger.valueOf(0));
        counters.setNrOfValidationErrors(BigInteger.valueOf(1));
        counters.setNrOfValidationInfos(BigInteger.valueOf(0));
        counters.setNrOfValidationWarnings(BigInteger.valueOf(0));
        mdaValidationResult.setValidationCounters(counters);

        return mdaValidationResult;
    }

    /**
     * Return the literal value of the message to pur in the report
     *
     * @param e       exception to report
     * @param message message to display with the error
     * @return literal value of the message.
     */
    private String getExceptionReportMessage(String message, Exception e) {
        StringBuilder finalMessage = new StringBuilder(message);
        if (e.getMessage() != null && !e.getMessage().isEmpty()) {
            finalMessage.append(String.format("with message : [ %s ]", e.getMessage()));
        }
        return finalMessage.toString();
    }

    private String convertFhirPathIntoXPath(String fhirPath) {
        String xPath;
        xPath = '/' + fhirPath.replace('.','/');
        return xPath;
    }
}
